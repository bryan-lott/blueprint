blueprint

## Development

### Installation

```bash
pip install -r requirements.txt
pip install ./wheels/Kivy-2.0.0rc1.20200428.4b1e5bfc2-cp38-cp38-macosx_10_13_x86_64.macosx_10_9_intel.macosx_10_9_x86_64.macosx_10_10_intel.macosx_10_10_x86_64.whl
```

### Notes

- UI
  - [Remi](https://github.com/dddomodossola/remi)
  - [Kivy](https://kivy.org/#home)
    - what a pain to get setup and running, especially on osx
  - [wxPython](https://www.wxpython.org/)
    - this is what I used last time to build the schema searcher
