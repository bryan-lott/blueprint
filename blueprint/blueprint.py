"""Main code for Blueprint, it's dangerous to go alone, run this!"""
import yaml
import logging
import pymysql
import os
from typing import Dict, List

logging.basicConfig()
log = logging.getLogger()

DB_HOST = os.environ.get("DB_HOST", "")
DB_PORT = os.environ.get("DB_PORT", "")
DB_USER = os.environ.get("DB_USER", "")
DB_PASS = os.environ.get("DB_PASS", "")
DB_NAME = os.environ.get("DB_NAME", "")


def read_config(db_config_path):
    with open(db_config_path) as infile:
        return yaml.load(infile)


def read_metadata(query: str) -> List[Dict[str, List[str, str, str, str]]]:
    with pymysql.connect(
        host=DB_HOST, port=DB_PORT, user=DB_USER, password=DB_PASS, db=DB_NAME
    ) as con:
        cur = con.cursor(pymysql.cursors.DictCursor)
        cur.execute(query)
        return cur.fetchall()


def search(metadata, input_value, input_type):
    pass


def main():
    db_config = read_config("./config/db.yaml")
    metadata = read_metadata(db_config["mysql"]["query"]["metadata"])

    # get input search value
    # get input search type
    # go query
    # return results


if __name__ == "__main__":
    main()
